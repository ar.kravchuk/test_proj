def fib(n):
    prev, current = 1, 1
    if n <= 2:
        return current
    for i in range(2, n + 1):
        prev, current = current, prev + current
    return current
